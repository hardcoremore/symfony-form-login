<?php

namespace Test\ChakyTestAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;


/**
 * User
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="user_username", columns={"username"}),
 *                                                  @ORM\UniqueConstraint(name="user_email", columns={"email"})})
 * @ORM\Entity(repositoryClass="Test\ChakyTestAppBundle\Entity\Repository\UserRepository")
 *
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements AdvancedUserInterface, EquatableInterface, \Serializable
{
    public function __construct()
    {
        $this->setSalt(sha1(uniqid(null, true) . time()));
        $this->setCreatedDatetime(new \DateTime());

        $this->createdSmartMeters = new ArrayCollection();
        $this->lastEditedSmartMeters = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=64)
     */
    private $username;


    /**
     *  @var string
     *
     * @ORM\Column(name="salt", type="string", length=40)
     */
    private $salt;


    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=88)
     */
    private $password;

     /**
     * @var datetime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
    *
    * @ORM\OneToMany(targetEntity="Blog", mappedBy="createdBy")
    *
    */
    private $createdBlogs;

    /**
    *
    * @ORM\OneToMany(targetEntity="Blog", mappedBy="lastEditedBy")
    *
    */
    private $lastEditedBlogs;


    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return true;
    }

    public function getRoles()
    {
       return array('ROLE_SUPER_ADMIN');
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->username,
            $this->email,
            $this->isActive
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->username,
            $this->email,
            $this->isActive
        ) = unserialize($serialized);
    }

    

    public function isEqualTo(UserInterface $user)
    {
        return $this->id === $user->getId();
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return SiteUser
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return SiteUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return SiteUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return SiteUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return SiteUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return SiteUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return SiteUser
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return SiteUser
     */
    public function setIsActive($isActive)
    {
        if(gettype($isActive) === "string")
        {
            $this->isActive = ($isActive === 'true');
        }
        else
        {
            $this->isActive = (bool)$isActive;
        }

        return $this;
    }

    /**
     * Get isActive
     *
     * @return booelan
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Get ownedRoles
     *
     * @return ArrayCollection
     */
    public function getOwnedRoles()
    {
        return $this->ownedRoles;
    }

    /**
     * Add createdBlogs
     *
     * @param \Test\ChakyTestAppBundle\Entity\Blog $createdBlogs
     * @return User
     */
    public function addCreatedBlog(\Test\ChakyTestAppBundle\Entity\Blog $createdBlogs)
    {
        $this->createdBlogs[] = $createdBlogs;

        return $this;
    }

    /**
     * Remove createdBlogs
     *
     * @param \Test\ChakyTestAppBundle\Entity\Blog $createdBlogs
     */
    public function removeCreatedBlog(\Test\ChakyTestAppBundle\Entity\Blog $createdBlogs)
    {
        $this->createdBlogs->removeElement($createdBlogs);
    }

    /**
     * Get createdBlogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedBlogs()
    {
        return $this->createdBlogs;
    }

    /**
     * Add lastEditedBlogs
     *
     * @param \Test\ChakyTestAppBundle\Entity\Blog $lastEditedBlogs
     * @return User
     */
    public function addLastEditedBlog(\Test\ChakyTestAppBundle\Entity\Blog $lastEditedBlogs)
    {
        $this->lastEditedBlogs[] = $lastEditedBlogs;

        return $this;
    }

    /**
     * Remove lastEditedBlogs
     *
     * @param \Test\ChakyTestAppBundle\Entity\Blog $lastEditedBlogs
     */
    public function removeLastEditedBlog(\Test\ChakyTestAppBundle\Entity\Blog $lastEditedBlogs)
    {
        $this->lastEditedBlogs->removeElement($lastEditedBlogs);
    }

    /**
     * Get lastEditedBlogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLastEditedBlogs()
    {
        return $this->lastEditedBlogs;
    }
}
