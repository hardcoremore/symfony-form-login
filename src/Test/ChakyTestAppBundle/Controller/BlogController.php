<?php

namespace Test\ChakyTestAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlogController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $blogs = $em->getRepository("ChakyTestAppBundle:Blog")->findAll();

        return $this->render('ChakyTestAppBundle:Blog:index.html.twig', array("blogs" => $blogs));
    }
}
