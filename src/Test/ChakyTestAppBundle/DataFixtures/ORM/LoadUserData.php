<?php

namespace Test\ChakyTestAppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Test\ChakyTestAppBundle\Entity\User;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    static $NUMBER_OF_USERS = 0;

     /**
     * @var ContainerInterface
     */
    private $container;

    private $firstNames = array(
        'Bayo',
        'Taiwo',
        'Wumi',
        'Lukman',
        'Vladimir',
        'Damir',
        'Vladimir',
        'Dragomir',
        'Andrija',
    );

    private $lastNames = array(
        'Okunowo',
        'Akinseye',
        'Ogheotuoma',
        'Ottun',
        'Trbovic',
        'Mehic',
        'Marinovic',
        'Krstic',
        'Bednarik'
    );

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setFirstName('Super');
        $userAdmin->setLastName('Admin');
        $userAdmin->setUsername('superadmin');
        $userAdmin->setEmail('superadmin@email.com');
        $userAdmin->setIsActive(true);

        $this->setUserPassword($userAdmin, 'superadmin');

        $manager->persist($userAdmin);
        $manager->flush();

        // store reference to admin role for User relation to Role
        $this->addReference('super-admin-user', $userAdmin);

        $user = null;

        $random = 0;

        $numberOfOptions = count($this->firstNames);

        self::$NUMBER_OF_USERS = $numberOfOptions + 1;
        
        for($i = 0; $i < $numberOfOptions; $i++)
        {
            $user = new User();

            $user->setFirstName($this->firstNames[$i]);
            $user->setLastName($this->lastNames[$i]);

            $user->setUsername($this->createUsername($user));
            $user->setEmail($this->createEmail($user));

            $this->setUserPassword($user, $user->getUsername());

            $user->setIsActive(true);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function createUsername(User $user)
    {
        $firstName = strtolower($user->getFirstName());
        $firstName = substr($firstName, 0, 1);

        return $firstName . strtolower($user->getLastName());
    }

    public function createEmail(User $user)
    {
        return strtolower($user->getFirstName()) . '.' . strtolower($user->getLastName()) . "@email.com";
    }

    public function setUserPassword(User $user, $password)
    {
        $passwordEncoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPassword = $passwordEncoder->encodePassword($password, $user->getSalt());

        $user->setPassword($encodedPassword);
    }
}