<?php

namespace Test\ChakyTestAppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Test\ChakyTestAppBundle\Entity\Blog;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadBlogData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    static $NUMBER_OF_BLOGS = 15;

     /**
     * @var ContainerInterface
     */
    private $container;


    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $category = null;

        for($i = 0; $i < self::$NUMBER_OF_BLOGS; $i++)
        {
            $blog = new Blog();

            $blog->setTitle('Title ' . ($i + 1));
            $blog->setSlug('title-' . ($i + 1));

            $blog->setBody("This is content of blog: " . ($i + 1));

            $category = $manager->getRepository('ChakyTestAppBundle:Category')->find(rand(1, 5));

            $blog->setCategory($category);

            $manager->persist($blog);
        }

        $manager->flush();
    }
}