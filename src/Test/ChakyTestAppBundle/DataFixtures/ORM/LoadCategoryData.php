<?php

namespace Test\ChakyTestAppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Test\ChakyTestAppBundle\Entity\Category;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCategoryData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

     /**
     * @var ContainerInterface
     */
    private $container;

    private $categories = array(
        'Life',
        'Tech',
        'IT',
        'Food',
        'Sport'
    );

    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $categoriesLen = count($this->categories);

        for($i = 0; $i < $categoriesLen; $i++)
        {
            $category = new Category();

            $category->setName($this->categories[$i]);

            $manager->persist($category);
        }

        $manager->flush();
    }
}